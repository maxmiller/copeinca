INSERT INTO django_content_type (id, app_label, model) VALUES (1, 'admin', 'logentry');
INSERT INTO django_content_type (id, app_label, model) VALUES (2, 'auth', 'permission');
INSERT INTO django_content_type (id, app_label, model) VALUES (3, 'auth', 'group');
INSERT INTO django_content_type (id, app_label, model) VALUES (4, 'auth', 'user');
INSERT INTO django_content_type (id, app_label, model) VALUES (5, 'contenttypes', 'contenttype');
INSERT INTO django_content_type (id, app_label, model) VALUES (6, 'sessions', 'session');
INSERT INTO django_content_type (id, app_label, model) VALUES (7, 'despachos', 'servidor');
INSERT INTO django_content_type (id, app_label, model) VALUES (8, 'despachos', 'bolsista');
INSERT INTO django_content_type (id, app_label, model) VALUES (9, 'despachos', 'banco');
INSERT INTO django_content_type (id, app_label, model) VALUES (10, 'despachos', 'projeto');
INSERT INTO django_content_type (id, app_label, model) VALUES (11, 'despachos', 'despachoprocesso');
INSERT INTO django_content_type (id, app_label, model) VALUES (12, 'frontend', 'module');
INSERT INTO django_content_type (id, app_label, model) VALUES (13, 'despachos', 'edital');