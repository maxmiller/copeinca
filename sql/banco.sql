CREATE TABLE auth_group
(
    id INTEGER PRIMARY KEY NOT NULL,
    name TEXT NOT NULL
);
CREATE UNIQUE INDEX sqlite_autoindex_auth_group_1 ON auth_group (name);
CREATE TABLE auth_group_permissions
(
    id INTEGER PRIMARY KEY NOT NULL,
    group_id INTEGER NOT NULL,
    permission_id INTEGER NOT NULL,
    FOREIGN KEY (group_id) REFERENCES auth_group (id) DEFERRABLE INITIALLY DEFERRED,
    FOREIGN KEY (permission_id) REFERENCES auth_permission (id) DEFERRABLE INITIALLY DEFERRED
);
CREATE UNIQUE INDEX auth_group_permissions_group_id_0cd325b0_uniq ON auth_group_permissions (group_id, permission_id);
CREATE TABLE auth_permission
(
    id INTEGER PRIMARY KEY NOT NULL,
    content_type_id INTEGER NOT NULL,
    codename TEXT NOT NULL,
    name TEXT NOT NULL,
    FOREIGN KEY (content_type_id) REFERENCES django_content_type (id) DEFERRABLE INITIALLY DEFERRED
);
CREATE UNIQUE INDEX auth_permission_content_type_id_01ab375a_uniq ON auth_permission (content_type_id, codename);
CREATE TABLE auth_user
(
    id INTEGER PRIMARY KEY NOT NULL,
    password TEXT NOT NULL,
    last_login TEXT,
    is_superuser INTEGER NOT NULL,
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL,
    email TEXT NOT NULL,
    is_staff INTEGER NOT NULL,
    is_active INTEGER NOT NULL,
    date_joined TEXT NOT NULL,
    username TEXT NOT NULL
);
CREATE UNIQUE INDEX sqlite_autoindex_auth_user_1 ON auth_user (username);
CREATE TABLE auth_user_groups
(
    id INTEGER PRIMARY KEY NOT NULL,
    user_id INTEGER NOT NULL,
    group_id INTEGER NOT NULL,
    FOREIGN KEY (user_id) REFERENCES auth_user (id) DEFERRABLE INITIALLY DEFERRED,
    FOREIGN KEY (group_id) REFERENCES auth_group (id) DEFERRABLE INITIALLY DEFERRED
);
CREATE UNIQUE INDEX auth_user_groups_user_id_94350c0c_uniq ON auth_user_groups (user_id, group_id);
CREATE TABLE auth_user_user_permissions
(
    id INTEGER PRIMARY KEY NOT NULL,
    user_id INTEGER NOT NULL,
    permission_id INTEGER NOT NULL,
    FOREIGN KEY (user_id) REFERENCES auth_user (id) DEFERRABLE INITIALLY DEFERRED,
    FOREIGN KEY (permission_id) REFERENCES auth_permission (id) DEFERRABLE INITIALLY DEFERRED
);
CREATE UNIQUE INDEX auth_user_user_permissions_user_id_14a6b632_uniq ON auth_user_user_permissions (user_id, permission_id);
CREATE TABLE despachos_banco
(
    nome TEXT NOT NULL,
    codigo TEXT PRIMARY KEY NOT NULL
);
CREATE TABLE despachos_bolsista
(
    id INTEGER PRIMARY KEY NOT NULL,
    agencia TEXT NOT NULL,
    conta TEXT NOT NULL,
    operacao TEXT,
    nome TEXT NOT NULL,
    banco_id TEXT NOT NULL,
    projeto_id INTEGER NOT NULL,
    valor INTEGER,
    cpf TEXT NOT NULL,
    FOREIGN KEY (banco_id) REFERENCES despachos_banco (codigo) DEFERRABLE INITIALLY DEFERRED,
    FOREIGN KEY (projeto_id) REFERENCES despachos_projeto (id) DEFERRABLE INITIALLY DEFERRED
);
CREATE UNIQUE INDEX sqlite_autoindex_despachos_bolsista_1 ON despachos_bolsista (cpf);
CREATE TABLE despachos_despachoprocesso
(
    processo TEXT PRIMARY KEY NOT NULL,
    data_processo TEXT NOT NULL,
    data_despacho TEXT NOT NULL,
    projeto_id INTEGER NOT NULL,
    solicitante_id TEXT NOT NULL,
    periodo_bolsa_fim TEXT,
    periodo_bolsa_inicio TEXT,
    edital_id INTEGER,
    FOREIGN KEY (projeto_id) REFERENCES despachos_projeto (id) DEFERRABLE INITIALLY DEFERRED,
    FOREIGN KEY (solicitante_id) REFERENCES despachos_servidor (siape) DEFERRABLE INITIALLY DEFERRED,
    FOREIGN KEY (edital_id) REFERENCES despachos_edital (id) DEFERRABLE INITIALLY DEFERRED
);
CREATE TABLE despachos_edital
(
    id INTEGER PRIMARY KEY NOT NULL,
    numero INTEGER NOT NULL,
    ano INTEGER NOT NULL,
    setor TEXT
);
CREATE TABLE despachos_projeto
(
    id INTEGER PRIMARY KEY NOT NULL,
    data_inicio TEXT NOT NULL,
    data_fim TEXT NOT NULL,
    coordenador_id TEXT NOT NULL,
    projeto TEXT NOT NULL,
    FOREIGN KEY (coordenador_id) REFERENCES despachos_servidor (siape) DEFERRABLE INITIALLY DEFERRED
);
CREATE TABLE despachos_servidor
(
    siape TEXT PRIMARY KEY NOT NULL,
    nome TEXT NOT NULL,
    email TEXT NOT NULL
);
CREATE TABLE django_admin_log
(
    id INTEGER PRIMARY KEY NOT NULL,
    object_id TEXT,
    object_repr TEXT NOT NULL,
    action_flag INTEGER NOT NULL,
    change_message TEXT NOT NULL,
    content_type_id INTEGER,
    user_id INTEGER NOT NULL,
    action_time TEXT NOT NULL,
    FOREIGN KEY (content_type_id) REFERENCES django_content_type (id) DEFERRABLE INITIALLY DEFERRED,
    FOREIGN KEY (user_id) REFERENCES auth_user (id) DEFERRABLE INITIALLY DEFERRED
);
CREATE TABLE django_content_type
(
    id INTEGER PRIMARY KEY NOT NULL,
    app_label TEXT NOT NULL,
    model TEXT NOT NULL
);
CREATE UNIQUE INDEX django_content_type_app_label_76bd3d3b_uniq ON django_content_type (app_label, model);
CREATE TABLE django_migrations
(
    id INTEGER PRIMARY KEY NOT NULL,
    app TEXT NOT NULL,
    name TEXT NOT NULL,
    applied TEXT NOT NULL
);
CREATE TABLE django_session
(
    session_key TEXT PRIMARY KEY NOT NULL,
    session_data TEXT NOT NULL,
    expire_date TEXT NOT NULL
);
CREATE UNIQUE INDEX django_session_de54fa62 ON django_session (expire_date);
CREATE TABLE frontend_module
(
    id INTEGER PRIMARY KEY NOT NULL,
    label TEXT NOT NULL,
    installed INTEGER NOT NULL
);
CREATE UNIQUE INDEX frontend_module_d304ba20 ON frontend_module (label);
CREATE TABLE sqlite_sequence
(
    name TEXT,
    seq TEXT
);