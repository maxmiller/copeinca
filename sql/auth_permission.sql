INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (1, 1, 'add_logentry', 'Can add log entry');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (2, 1, 'change_logentry', 'Can change log entry');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (3, 1, 'delete_logentry', 'Can delete log entry');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (4, 2, 'add_permission', 'Can add permission');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (5, 2, 'change_permission', 'Can change permission');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (6, 2, 'delete_permission', 'Can delete permission');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (7, 3, 'add_group', 'Can add group');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (8, 3, 'change_group', 'Can change group');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (9, 3, 'delete_group', 'Can delete group');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (10, 4, 'add_user', 'Can add user');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (11, 4, 'change_user', 'Can change user');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (12, 4, 'delete_user', 'Can delete user');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (13, 5, 'add_contenttype', 'Can add content type');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (14, 5, 'change_contenttype', 'Can change content type');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (15, 5, 'delete_contenttype', 'Can delete content type');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (16, 6, 'add_session', 'Can add session');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (17, 6, 'change_session', 'Can change session');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (18, 6, 'delete_session', 'Can delete session');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (19, 7, 'add_servidor', 'Can add servidor');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (20, 7, 'change_servidor', 'Can change servidor');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (21, 7, 'delete_servidor', 'Can delete servidor');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (22, 8, 'add_bolsista', 'Can add bolsista');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (23, 8, 'change_bolsista', 'Can change bolsista');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (24, 8, 'delete_bolsista', 'Can delete bolsista');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (25, 9, 'add_banco', 'Can add banco');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (26, 9, 'change_banco', 'Can change banco');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (27, 9, 'delete_banco', 'Can delete banco');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (28, 10, 'add_projeto', 'Can add projeto');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (29, 10, 'change_projeto', 'Can change projeto');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (30, 10, 'delete_projeto', 'Can delete projeto');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (31, 11, 'add_despacho', 'Can add despacho');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (32, 11, 'change_despacho', 'Can change despacho');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (33, 11, 'delete_despacho', 'Can delete despacho');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (34, 12, 'add_module', 'Can add module');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (35, 12, 'change_module', 'Can change module');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (36, 12, 'delete_module', 'Can delete module');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (37, 11, 'add_despachoprocesso', 'Can add despacho processo');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (38, 11, 'change_despachoprocesso', 'Can change despacho processo');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (39, 11, 'delete_despachoprocesso', 'Can delete despacho processo');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (40, 13, 'add_edital', 'Can add edital');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (41, 13, 'change_edital', 'Can change edital');
INSERT INTO auth_permission (id, content_type_id, codename, name) VALUES (42, 13, 'delete_edital', 'Can delete edital');