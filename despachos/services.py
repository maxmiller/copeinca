import requests

def loginSUAP(login,password):
    url = 'https://suap.ifrn.edu.br/api/autenticacao/token/'
    params = {'username': login, 'password': password}
    r = requests.post(url, data=params)
    token = r.json()
    print(token)
    return token['token'] if 'token' in token else None
