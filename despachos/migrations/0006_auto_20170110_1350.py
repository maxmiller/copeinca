# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-01-10 16:50
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('despachos', '0005_auto_20170110_0953'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Despacho',
            new_name='DespachoProcesso',
        ),
    ]
