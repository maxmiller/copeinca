from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(Servidor)
admin.site.register(Projeto)
admin.site.register(Bolsista)
admin.site.register(DespachoProcesso)
admin.site.register(Banco)
admin.site.register(Edital)
