from django import forms
from .models import Servidor
from .models import Projeto
from .models import Banco
from .models import Bolsista
from .models import DespachoProcesso
from .models import Edital
from .models import Planejamento,Orcamento,CotacaoDolar
from .choices import *
from django.forms.extras.widgets import SelectDateWidget
from django.utils import formats
from material import Layout, Row, Column,Span6
import datetime
from datetime import date
from material import Layout, Row, Column, Fieldset, Span2, Span3, Span5, Span6, Span10,Span12
from djmoney.forms import MoneyField


class ServidorForm(forms.ModelForm):
    class Meta:
        model = Servidor
        fields = ('siape', 'nome','email',)

class ProjetoForm(forms.ModelForm):

    coordenador = forms.ModelChoiceField(queryset=Servidor.objects.all(),widget=forms.Select)
    data_inicio = forms.DateField()
    data_fim = forms.DateField()
    finalizado = forms.NullBooleanField()

    class Meta:
        model = Projeto
        fields = ('coordenador', 'projeto','data_inicio', 'data_fim','finalizado',)


class BolsistaForm(forms.ModelForm):
    projeto = forms.ModelChoiceField(queryset=Projeto.objects.all(),widget=forms.Select)
    banco = forms.ModelChoiceField(queryset=Banco.objects.order_by('codigo'),widget=forms.Select)

    class Meta:
        model = Bolsista
        fields = ('projeto', 'banco','agencia','conta','operacao','nome','cpf','valor',)


class DespachoProcessoForm(forms.ModelForm):
    projeto = forms.ModelChoiceField(queryset=Projeto.objects.filter(finalizado = False),widget=forms.Select)
    solicitante = forms.ModelChoiceField(queryset=Servidor.objects.filter(siape__in = Projeto.objects.all().values_list('coordenador',flat = True)).order_by('nome'),widget=forms.Select)
    edital = forms.ModelChoiceField(queryset=Edital.objects.order_by('numero','ano'),widget=forms.Select)
    data_processo = forms.DateField(widget=forms.DateInput(attrs={'type':'date', 'class':'datepicker'}) )
    data_despacho = forms.DateField(widget=forms.DateInput(attrs={'type':'date','value': formats.date_format(date.today(), "d/m/Y"), 'class':'datepicker'})  )
    periodo_bolsa_inicio = forms.DateField(widget=forms.DateInput(attrs={'type':'date', 'class':'datepicker'}) )
    periodo_bolsa_fim = forms.DateField(widget=forms.DateInput(attrs={'type':'date', 'class':'datepicker'}) )
    processo = forms.CharField()

    class Meta:
        model = DespachoProcesso

        fields = ('edital','projeto','processo', 'solicitante', 'data_processo', 'periodo_bolsa_inicio','periodo_bolsa_fim' ,'data_despacho',)


class LoginForm(forms.Form):
    username  = forms.CharField()
    password  = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        fields = ('username', 'password',)

class OrcamentoForm(forms.ModelForm):
    ano=forms.ChoiceField(choices=YEAR_CHOICES,widget=forms.Select(),required=True)
    semestre=forms.ChoiceField(choices = SEMESTRE_CHOICES,widget=forms.Select(),required=True)
    orcamento = MoneyField(default_currency='BRL')
    inicio=forms.DateField(widget=forms.DateInput(attrs={'type':'date', 'class':'datepicker'}) )
    fim=forms.DateField(widget=forms.DateInput(attrs={'type':'date', 'class':'datepicker'}) )
    class Meta:
        model = Orcamento
        fields = ("ano","semestre","orcamento","inicio","fim",)

class PlanejamentoForm(forms.ModelForm):
    projetos = forms.ChoiceField(choices=BOOL_CHOICES, widget=forms.Select(),required=True)
    estudando = forms.ChoiceField(choices=BOOL_CHOICES, widget=forms.Select(),required=True)
    numero_eventos_brasil = forms.IntegerField()
    numero_eventos_exterior = forms.IntegerField()
    titulacao = forms.ChoiceField(choices=TITULACAO_CHOICES, widget=forms.Select(),required=True)
    ingresso_ifrn = forms.DateField(widget=forms.DateInput(attrs={'type':'date', 'class':'datepicker'}) )
    nome_evento = forms.CharField()
    justificativa_eventos = forms.CharField(widget=forms.TextInput)
    data_inicio_evento = forms.DateField(widget=forms.DateInput(attrs={'type':'date', 'class':'datepicker'}) )
    data_fim_evento = forms.DateField(widget=forms.DateInput(attrs={'type':'date', 'class':'datepicker'}) )
    prioridade_evento = forms.IntegerField()
    tipo_evento_capacitacao = forms.ChoiceField(choices=EVENTOS_CHOICES,widget=forms.Select(),required=True)
    semestre = forms.ModelChoiceField(queryset=Orcamento.objects.filter(inicio__lte=datetime.datetime.now(), fim__gte=datetime.datetime.now()),widget=forms.Select())
    relevancia = forms.ChoiceField(choices=BOOL_CHOICES, widget=forms.Select(),required=True)
    local_evento = forms.ChoiceField(choices=LOCAL_CHOICES, widget=forms.Select(),required=True)

    class Meta:
        model = Planejamento
        fields = ('prioridade_evento','nome_evento','justificativa_eventos','data_inicio_evento','data_fim_evento','local_evento','tipo_evento_capacitacao', 'semestre',  'relevancia','projetos','estudando','numero_eventos_brasil', 'numero_eventos_exterior', 'titulacao','ingresso_ifrn',)

class CotacaoForm(forms.ModelForm):
    valor = MoneyField(default_currency='BRL')

    class Meta:
        model = CotacaoDolar
        fields = ('valor',)
