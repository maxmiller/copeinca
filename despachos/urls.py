from django.conf.urls import include, url
from . import views

app_name = 'despachos'
urlpatterns = [
    url(r'^$', views.principal, name="principal"),
    url(r'^login/$', views.login ,name='login'),
    url(r'^logout/$', views.logout,name='logout'),
    url(r'^servidor/$', views.servidor_list, name="servidor"),
    url(r'^servidor/new/$', views.servidor_new, name='servidor_new'),
    url(r'^servidor/edit/(?P<pk>[0-9]+)/$', views.servidor_edit, name='servidor_edit'),
    url(r'^projeto/$', views.projeto_list, name="projeto"),
    url(r'^projeto/new/$', views.projeto_new, name='projeto_new'),
    url(r'^projeto/edit/(?P<pk>[0-9]+)/$', views.projeto_edit, name='projeto_edit'),
    url(r'^bolsista/$', views.bolsista_list, name="bolsista"),
    url(r'^bolsista/new/$', views.bolsista_new, name='bolsista_new'),
    url(r'^despacho_processo/$', views.despacho_processo_list, name='despacho_processo'),
    url(r'^despacho_processo/new/$', views.despacho_processo_new, name='despacho_processo_new'),
    url(r'^despacho_processo/edit/(?P<pk>[0-9]{5}.[0-9]{6}.[0-9]{4}-[0-9]{2})/$', views.despacho_processo_edit, name='despacho_processo_edit'),
    url(r'^despacho_processo/delete/(?P<pk>[0-9]{5}.[0-9]{6}.[0-9]{4}-[0-9]{2})/$', views.despacho_processo_delete, name='despacho_processo_delete'),
    url(r'^despacho_processo/print/(?P<pk>[0-9]{5}.[0-9]{6}.[0-9]{4}-[0-9]{2})/$', views.despacho_processo_print, name='despacho_processo_print'),
    url(r'^pagamentos/(?P<projeto>[0-9]+)/$', views.pagamentos, name='pagamentos'),
    url(r'^planejamento/$', views.planejamento_list, name='planejamento'),
    url(r'^orcamento/$', views.orcamento_list, name='orcamento'),
    url(r'^orcamento/new/$', views.orcamento_new, name='orcamento_new'),
    url(r'^planejamento/new/$', views.planejamento_new, name='planejamento_new'),
    url(r'^cotacao/$', views.cotacao_list, name="cotacao"),
    url(r'^cotacao/new/$', views.cotacao_new, name='cotacao_new'),
    url(r'^cotacao/edit/(?P<pk>[0-9]+)/$', views.cotacao_edit, name='cotacao_edit'),
    url(r'^planejamento/show/(?P<pk>[0-9]+)/$', views.planejamento_show, name='planejamento_show'),

    #url(r'^planejamento_semestre/$', views.planejamento_semetre, name='planejamento_semestre'),

    #url(r'^/$', views.index, name='index'),


#    url(r'^pcd/(?P<pk>[0-9]+)/$', views.sync_pcd, name='sync_pcd'),
]
