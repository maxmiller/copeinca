from reportlab.lib.pagesizes import letter, A4
from reportlab.platypus import SimpleDocTemplate, Paragraph
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_CENTER

from .models import DespachoProcesso
from .models import Servidor
from .models import Bolsista
from .models import Projeto
from .models import Banco

class DespachoPrint:
    def __init__(self, buffer):
        self.buffer = buffer
        self.pagesize = A4
        self.width, self.height = self.pagesize

    def print_despacho(self, despacho):
        buffer = self.buffer
        doc = SimpleDocTemplate(buffer,
                                rightMargin=72,
                                leftMargin=72,
                                topMargin=72,
                                bottomMargin=72,
                                pagesize=self.pagesize)

        # Our container for 'Flowable' objects
        elements = []

        # A large collection of style sheets pre-made for us
        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(name='centered', alignment=TA_CENTER))

        # Draw things on the PDF. Here's where the PDF generation happens.
        # See the ReportLab documentation for the full list of functionality.
        #print(Paragraph('Despacho'))

        elements.append(Paragraph('Despacho',styles['Heading1']))
        text = "Documento de Referência: Processo nº "+str(despacho.processo)+", de "+str(despacho.data_processo)
        elements.append(Paragraph(text,styles['Heading1']))

        #users = User.objects.all()
        #elements.append(Paragraph('My User Names', styles['Heading1']))
        #for i, user in enumerate(users):
    #        elements.append(Paragraph(user.get_full_name(), styles['Normal']))

        doc.build(elements)

        # Get the value of the BytesIO buffer and write it to the response.
        pdf = buffer.getvalue()
        print (pdf)
        buffer.close()
        return pdf
