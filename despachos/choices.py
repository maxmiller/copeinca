
SEMESTRE_UM = 1
SEMESTRE_DOIS = 2
SEMESTRE_CHOICES = (
    (SEMESTRE_UM, '1º Semestre'),
    (SEMESTRE_DOIS, '2º Semestre'),
)
YEAR_CHOICES = []
for r in range(2018, 2030):
    YEAR_CHOICES.append((r,r))
SIM = 1
NAO = 2

BOOL_CHOICES = (
    (SIM, "Sim"),
    (NAO, "Não"),
)

GRADUCAO = 20
MESTRADO = 15
DOUTORADO = 10

TITULACAO_CHOICES = (
    (GRADUCAO , 'Até Graduação'),
    (MESTRADO, 'Especialização/Mestrado'),
    (DOUTORADO, 'Doutorado'),
)


CONNEPI_CONGIC = 15
EVENTO_INTERNACIONAL_BRASIL=	10
EVENTO_NACIONAL_NORDESTE=9
EVENTO_NACIONAL=8
EVENTO_INTERNACIONAL_AMERICA_SUL= 6
EVENTO_INTERNACIONAL =	4
EVENTOS_REGIONAIS =	4
CURSO_AREA_ATUACAO = 10
OUTROS_EVENTOS_CAPACITACAO = 3

EVENTOS_CHOICES=(
    (CONNEPI_CONGIC ,'CONNEPI & CONGIC '),
    (EVENTO_INTERNACIONAL_BRASIL, 'Eventos Internacionais sediados no Brasil'),
    (EVENTO_NACIONAL_NORDESTE,'Eventos Nacionais sediados no Nordeste'),
    (EVENTO_NACIONAL, 'Eventos Nacionais em outras regiões'),
    (EVENTO_INTERNACIONAL_AMERICA_SUL, 'Eventos Internacionais sediados na América do Sul'),
    (EVENTO_INTERNACIONAL, 'Eventos Internacionais sediados fora da América do Sul'),
    (EVENTOS_REGIONAIS, 'Eventos Regionais sediados no Nordeste'),
    (CURSO_AREA_ATUACAO ,'Curso VOLTADO PARA AS ATIVIDADES específicas da área de atuação do servidor no Campus'),
    (OUTROS_EVENTOS_CAPACITACAO, 'Demais eventos/capacitações'),
)

NACIONAL = 0
INTERNACIONAL_AMERICA_SUL= 1
INTERNACIONAL = 2

LOCAL_CHOICES = (
(NACIONAL, 'Brasil'),
(INTERNACIONAL_AMERICA_SUL, 'América do Sul'),
(INTERNACIONAL, 'Fora da América do Sul'),
)
