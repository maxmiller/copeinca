from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render,redirect,get_object_or_404
from django.http import HttpResponse
#from django.sessions import sessions

from .models import Servidor,Projeto,Bolsista,DespachoProcesso,Planejamento, Orcamento,CotacaoDolar

from .forms import ServidorForm
from .forms import ProjetoForm
from .forms import BolsistaForm
from .forms import DespachoProcessoForm
from .forms import LoginForm
from .forms import OrcamentoForm,PlanejamentoForm,CotacaoForm
from . import services
#from .print import DespachoPrint
from django.contrib.auth.decorators import login_required
from io import BytesIO
# Create your views here.

from django.contrib.auth import login as auth_login, authenticate, logout as auth_logout
from django.contrib.auth.models import User

from django import template
from django.contrib.auth.models import Group
from django.core.exceptions import ObjectDoesNotExist



@login_required(login_url='despachos:login')
def principal(request):
    return render(request, 'despachos/principal.html')

def logout(request):
    auth_logout(request)
    return redirect('/')

def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST or None)
        if form.is_valid():
            form.username = request.POST['username']
            form.password = request.POST['password']
            if services.loginSUAP(form.username,form.password) is not None:
                user = authenticate(request=request,username=form.username, password=form.password)
                if user is not None:
                    auth_login(request, user)
                    return redirect('/')
                else:
                    try:
                        user = User.objects.get(username = form.username)
                        if not user is None:
                            user.set_password(form.password)
                            user.save()
                    except ObjectDoesNotExist:
                        user = User.objects.create_user(username = form.username, password = form.password)
                    user = authenticate(request=request,username=form.username, password=form.password)
                    if user is not None:
                        auth_login(request, user)
                        return redirect('/')

            else:
                return redirect('/')
    form = LoginForm()
    return render(request, 'despachos/login.html', { 'form': form })


@login_required(login_url='despachos:login')
def servidor_list(request):
    servidores = Servidor.objects.all()
    paginator = Paginator(servidores, 20) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        servidores = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        servidores = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        servidores = paginator.page(paginator.num_pages)

    return render(request, 'despachos/servidor_list.html', {'servidores':servidores})

@login_required(login_url='despachos:login')
def servidor_new(request):
    if(request.method == "POST"):
        form = ServidorForm(request.POST)
        if form.is_valid():
            servidor  = form.save(commit=False)
            servidor.save()
            return redirect('/servidor')
    else:
        form = ServidorForm()
    return render(request, 'despachos/servidor_form.html',{'form': form})

@login_required(login_url='despachos:login')
def servidor_edit(request,pk):
    servidor = get_object_or_404(Servidor, pk=pk)
    if(request.method == "POST"):
        form = ServidorForm(request.POST, instance = servidor)
        if form.is_valid():
            servidor = form.save(commit=False)
            servidor.save()
            return redirect('/servidor')
    else:
        form = ServidorForm(instance=servidor)
    return render(request, 'despachos/servidor_form.html',{'form': form})

@login_required(login_url='despachos:login')
def projeto_list(request):
    projetos = Projeto.objects.all()
    paginator = Paginator(projetos, 20) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        projetos = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        projetos = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        projetos = paginator.page(paginator.num_pages)

    return render(request, 'despachos/projeto_list.html', {'projetos':projetos})

@login_required(login_url='despachos:login')
def projeto_new(request):
    if(request.method == "POST"):
        form = ProjetoForm(request.POST)
        if form.is_valid():
            projeto = form.save(commit=False)
            projeto.save()
            return redirect('/projeto')
    else:
        form = ProjetoForm()
    return render(request, 'despachos/projeto_form.html',{'form': form})

@login_required(login_url='despachos:login')
def bolsista_list(request):
    bolsistas = Bolsista.objects.order_by('projeto','valor')
    paginator = Paginator(bolsistas, 20) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        bolsistas = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        bolsistas = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        bolsistas = paginator.page(paginator.num_pages)

    return render(request, 'despachos/bolsista_list.html', {'bolsistas':bolsistas})

@login_required(login_url='despachos:login')
def bolsista_new(request):
    if(request.method == "POST"):
        form = BolsistaForm(request.POST)
        if form.is_valid():
            bolsista = form.save(commit=False)
            bolsista.save()
            return redirect('/bolsista')
    else:
        form = BolsistaForm()
    return render(request, 'despachos/bolsista_form.html',{'form': form})

@login_required(login_url='despachos:login')
def despacho_processo_list(request):
    ds = DespachoProcesso.objects.order_by('-data_despacho')
    paginator = Paginator(ds, 5) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        ds = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        ds = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        ds = paginator.page(paginator.num_pages)

    return render(request, 'despachos/despacho_processo_list.html', {'ds':ds})

@login_required(login_url='despachos:login')
def despacho_processo_new(request):
    #despacho = get_object_or_404(DespachoProcesso, pk=pk)
    if(request.method == "POST"):
        form = DespachoProcessoForm(request.POST)
        if form.is_valid():
            despacho = form.save(commit=False)
            despacho.save()
            return redirect('/despacho_processo')
    else:
        form = DespachoProcessoForm()
    return render(request, 'despachos/despacho_processo_form.html',{'form': form})

@login_required(login_url='despachos:login')
def despacho_processo_delete(request, pk):
    obj = DespachoProcesso.objects.get(pk=pk)
    obj.delete()
    return redirect('/despacho_processo')

@login_required(login_url='despachos:login')
def despacho_processo_print(request, pk):
    obj = DespachoProcesso.objects.get(pk=pk)
    bolsistas = Bolsista.objects.filter(projeto = obj.projeto)

    return render(request,'despachos/despacho_processo_view.html',{'d':obj , 'bolsistas':bolsistas})

@login_required(login_url='despachos:login')
def despacho_processo_edit(request,pk):
    despacho = get_object_or_404(DespachoProcesso, pk=pk)
    if(request.method == "POST"):
        form = DespachoProcessoForm(request.POST, instance = despacho)
        if form.is_valid():
            despacho = form.save(commit=False)
            despacho.save()
            return redirect('/despacho_processo')
    else:
        form = DespachoProcessoForm(instance=despacho)
    return render(request, 'despachos/despacho_processo_form.html',{'form': form})

@login_required(login_url='despachos:login')
def pagamentos(request,projeto):
    despachos = DespachoProcesso.objects.filter(projeto = projeto)
    paginator = Paginator(despachos, 10) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        despachos = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        despachos = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        despachos = paginator.page(paginator.num_pages)


    return render(request, 'despachos/pagamentos_list.html',{'despachos': despachos})

@login_required(login_url='despachos:login')
def projeto_edit(request,pk):
    projeto = get_object_or_404(Projeto, pk=pk)
    if(request.method == "POST"):
        form = ProjetoForm(request.POST, instance = projeto)
        if form.is_valid():
            projeto = form.save(commit=False)
            projeto.save()
            return redirect('/projeto')
    else:
        form = ProjetoForm(instance=projeto)
    return render(request, 'despachos/projeto_form.html',{'form': form})

@login_required(login_url='despachos:login')
def planejamento_list(request):
    pls = Planejamento.objects.all()
    print(pls)
    if not request.user.is_superuser:
        pls = Planejamento.objects.filter(servidor=request.user)

    paginator = Paginator(pls, 20) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        pls = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        pls = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        pls = paginator.page(paginator.num_pages)

    return render(request, 'despachos/planejamento_list.html', {'planejamentos':pls})

@login_required(login_url='despachos:login')
def orcamento_list(request):
    orcamentos = Orcamento.objects.all()
    paginator = Paginator(orcamentos, 20) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        orcamentos = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        orcamentos = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        orcamentos = paginator.page(paginator.num_pages)

    return render(request, 'despachos/orcamento_list.html', {'pss':orcamentos})

@login_required(login_url='despachos:login')
def orcamento_new(request):
    if(request.method == "POST"):
        form = OrcamentoForm(request.POST)
        if form.is_valid():
            orcamento  = form.save(commit=False)
            orcamento.save()
            return redirect('/orcamento')
    else:
        form = OrcamentoForm()
    return render(request, 'despachos/orcamento_form.html',{'form': form})

@login_required(login_url='despachos:login')
def planejamento_new(request):
    if(request.method == "POST"):
        form = PlanejamentoForm(request.POST)
        if form.is_valid():
            planejamento  = form.save(commit=False)
            planejamento.servidor = request.user
            planejamento.cotacao = CotacaoDolar.objects.all().first()
            planejamento.selecionado = False
            planejamento.custo = planejamento.calculo_custos()
            planejamento.total = planejamento.calculo_total()
            planejamento.save()
            return redirect('/planejamento')
    else:
        form = PlanejamentoForm()
    return render(request, 'despachos/planejamento_form.html',{'form': form})

#cotacao

@login_required(login_url='despachos:login')
def cotacao_list(request):
    cotacoes = CotacaoDolar.objects.all()

    return render(request, 'despachos/cotacao_list.html', {'cotacoes':cotacoes})

@login_required(login_url='despachos:login')
def cotacao_new(request):
    if(request.method == "POST"):
        form = CotacaoForm(request.POST)
        if form.is_valid():
            cotacao  = form.save(commit=False)
            cotacao.save()
            return redirect('/cotacao')
    else:
        form = CotacaoForm()
    return render(request, 'despachos/cotacao_form.html',{'form': form})

@login_required(login_url='despachos:login')
def cotacao_edit(request,pk):
    cotacao = get_object_or_404(CotacaoDolar, pk=pk)
    if(request.method == "POST"):
        form = CotacaoForm(request.POST, instance = cotacao)
        if form.is_valid():
            cotacao = form.save(commit=False)
            cotacao.save()
            return redirect('/cotacao')
    else:
        form = CotacaoForm(instance=cotacao)
    return render(request, 'despachos/cotacao_form.html',{'form': form})

@login_required(login_url='despachos:login')
def planejamento_show(request,pk):
    planejamento = get_object_or_404(Planejamento, pk=pk)
    return render(request, 'despachos/planejamento_show.html',{'planejamento': planejamento})
