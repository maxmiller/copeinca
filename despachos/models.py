from django.db import models
from djmoney.models.fields import MoneyField
from django.contrib.auth.models import User
import datetime
from .choices import *

class Servidor(models.Model):
    siape = models.CharField(max_length=8,primary_key=True)
    nome = models.CharField(max_length=250)
    email = models.CharField(max_length=100)

    def __str__(self):
        return self.nome

class Projeto(models.Model):
    coordenador = models.ForeignKey(Servidor)
    projeto  = models.CharField(max_length=500)
    data_inicio = models.DateField()
    data_fim = models.DateField()
    finalizado = models.BooleanField(default=False)

    def __str__(self):
        return self.projeto

class Banco(models.Model):
    codigo = models.CharField(max_length=5, primary_key=True)
    nome = models.CharField(max_length=150, null=False)
    def __str__(self):
        return self.codigo +" - "+self.nome

class Bolsista(models.Model):
    projeto = models.ForeignKey(Projeto)
    banco = models.ForeignKey(Banco)
    agencia = models.CharField(max_length=6)
    conta = models.CharField(max_length=14)
    operacao = models.CharField(max_length=4,null=True)
    nome = models.CharField(max_length=200)
    cpf = models.CharField(max_length=14)
    valor = models.IntegerField(null=True)

    def __str__(self):
        return self.nome

class Edital(models.Model):
    numero = models.IntegerField()
    ano = models.IntegerField()
    setor = models.CharField(max_length=25,null=True)

    def __str__(self):
        return str(self.numero) + "/" + str(self.ano) + "-"+self.setor

class DespachoProcesso(models.Model):
    processo = models.CharField(max_length=20,primary_key=True)
    data_processo = models.DateField(null=False)
    projeto = models.ForeignKey(Projeto)
    solicitante = models.ForeignKey(Servidor)
    data_despacho = models.DateField(null=False)
    periodo_bolsa_inicio= models.DateField(null=True)
    periodo_bolsa_fim = models.DateField(null=True)
    edital = models.ForeignKey(Edital,null=True)

    def __str__(self):
        return self.processo


class Orcamento(models.Model):
    ano = models.IntegerField(choices=YEAR_CHOICES)
    semestre = models.IntegerField(choices = SEMESTRE_CHOICES, default = SEMESTRE_UM)
    orcamento = MoneyField(max_digits=10, decimal_places=2, default_currency='BRL')
    inicio = models.DateField()
    fim = models.DateField()

    def __str__(self):
        return str(self.ano)+ '.'+ str(self.semestre)


class CotacaoDolar(models.Model):
    valor = MoneyField(max_digits=10, decimal_places=2, default_currency='BRL')

class Planejamento(models.Model):
    relevancia = models.IntegerField()
    projetos = models.IntegerField(choices=BOOL_CHOICES, default = NAO)
    estudando = models.IntegerField(choices=BOOL_CHOICES, default = NAO)
    numero_eventos_brasil = models.IntegerField()
    numero_eventos_exterior = models.IntegerField()
    titulacao = models.IntegerField(choices=TITULACAO_CHOICES, default = GRADUCAO)
    ingresso_ifrn = models.DateField()
    nome_evento = models.CharField(max_length=200)
    justificativa_eventos = models.TextField()
    data_inicio_evento = models.DateField()
    data_fim_evento = models.DateField()
    prioridade_evento = models.IntegerField()
    tipo_evento_capacitacao = models.IntegerField(choices=EVENTOS_CHOICES, default = CONNEPI_CONGIC)
    semestre = models.ForeignKey(Orcamento)
    servidor = models.ForeignKey(User)
    selecionado = models.BooleanField(default=False)
    local_evento = models.IntegerField(default=0)
    cotacao = models.ForeignKey(CotacaoDolar, null=True)
    custo = MoneyField(max_digits=10, decimal_places=2, default_currency='BRL')
    total = models.FloatField(default=0.0)

    @property
    def ntitulacao( self ):
        for i in TITULACAO_CHOICES:
            if i[0] == self.titulacao :
                return i[1]
        return 'N/A'
    @property
    def ntipo_evento_capacitacao( self ):
        for i in EVENTOS_CHOICES:
            if i[0] == self.tipo_evento_capacitacao :
                return i[1]
        return 'N/A'

    @property
    def nestudando( self ):
        for i in BOOL_CHOICES:
            if i[0] == self.estudando :
                return i[1]
        return 'N/A'

    @property
    def nprojetos( self ):
        for i in BOOL_CHOICES:
            if i[0] == self.projetos :
                return i[1]
        return 'N/A'

    @property
    def nrelevancia( self ):
        for i in BOOL_CHOICES:
            if i[0] == self.relevancia :
                return i[1]
        return 'N/A'

    @property
    def nlocal_evento( self ):
        for i in LOCAL_CHOICES:
            if i[0] == self.local_evento :
                return i[1]
        return 'N/A'
    
    def calculo_custos(self):
        valor_diaria = 210.00
        if self.local_evento == 1:
            valor_diaria = 180 * self.cotacao.valor
        elif self.local_evento == 2:
            valor_diaria = 270 * self.cotacao.valor
        dias = (self.data_fim_evento - self.data_inicio_evento).days
        dias += 1.5
        return dias * valor_diaria

    def calculo_semestres (self):
        hoje = datetime.datetime.now().date()
        dias = (hoje -  self.ingresso_ifrn).days
        if dias/180 > 8:
            return 8 * 2.5
        else:
            return int(dias/180)*2.5

    def calculo_total(self):
        total = self.calculo_semestres()+ self.titulacao+self.relevancia + self.projetos+self.estudando#+ (15 if self.numero_eventos_brasil == 0 else (10/self.numero_eventos_brasil))+ (15 if self.numero_eventos_exterior == 0 else (10/(self.numero_eventos_exterior*2)))+ self.tipo_evento_capacitacao
        return total
